//
//  SwipeCell.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 15/10/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit

enum BottomConstant: CGFloat {
    case show = 49
    case hide = -50
}

class SwipeCell: UICollectionViewCell {

    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var imageNews: UIImageView!
    @IBOutlet weak var labelHeading: UILabel!
    @IBOutlet weak var labelSourceNTime: UILabel!
    @IBOutlet weak var tabbarBottom: UITabBar!
    @IBOutlet weak var contraintHeightTabbar: NSLayoutConstraint!
    
    @IBOutlet weak var btnSourceNTime: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.showHideBottom(boolean: true)
        self.labelSourceNTime.text = ""
        self.contraintHeightTabbar.constant = BottomConstant.hide.rawValue
    }

    func updateCell(dict: NewsTableModel) {
//        let singleNews = NewsModel(o: dict)
        Common.share.loadImage(img: self.imageNews, url: URL(string: dict.image_url)!)
        
        self.labelHeading.text = dict.title
        self.labelDescription.text = dict.content
        self.btnSourceNTime.setTitle(dict.source_name, for: .normal)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.4
        
        let attributedString = NSMutableAttributedString(string: self.labelDescription.text!)
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        self.labelDescription.attributedText = attributedString
    }
    
    func showHideBottom(boolean: Bool) {
        self.contraintHeightTabbar.constant = boolean ? BottomConstant.hide.rawValue : BottomConstant.show.rawValue
        UIView.animate(withDuration: 0.34, delay: 0.0, options: .curveEaseInOut, animations: {
            self.bringSubview(toFront: self.tabbarBottom)
            self.layoutIfNeeded()
        }) { (finish) in
//            self.setNeedsDisplay()
            
//            self.tabbarBottom.isHidden = boolean
        }
    }
    
    @IBAction func btnSourceNTimeAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "displayWebview"), object: sender.tag)
        NotificationCenter.default.removeObserver(NSNotification.Name(rawValue: "displayWebview"))
    }
}
