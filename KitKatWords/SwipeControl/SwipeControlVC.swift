//
//  SwipeControlVC.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 15/10/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit

class SwipeControlVC: UIPageViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.dataSource = self
        self.delegate = self
        
        setViewControllers([orderedVC[0]],
                           direction: .forward,
                           animated: true,
                           completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private(set) lazy var orderedVC: [UIViewController] = {
        return [
//            self.VC(name: "RightVC") as! RightVC,
            self.VC(name: "SwipeCollectionVC") as! SwipeCollectionVC,
            SegueController.MainNavigationVC()
        ]
    }()
    
    private func VC(name: String) -> UIViewController {
        return (self.storyboard?.instantiateViewController(withIdentifier: name + "ID"))!
    }
}

extension SwipeControlVC: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedVC.index(of: viewController) else { return nil }
        
        
        
        let previousIndex = viewControllerIndex - 1
//        if viewControllerIndex == 1 {
//            let vc = self.orderedVC[1] as! SwipeVC
//            vc.swipeView.isHidden = true
//        }
//        else if previousIndex == 1 {
//            let vc = self.orderedVC[1] as! SwipeVC
//            vc.swipeView.isHidden = false
//        }
        guard previousIndex >= 0 else { return nil }
        
        guard orderedVC.count > previousIndex else { return nil }
        print("*****before next vc: ", orderedVC[previousIndex])
        
        return orderedVC[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedVC.index(of: viewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
//        if viewControllerIndex == 1 {
//            let vc = self.orderedVC[1] as! SwipeVC
//            vc.swipeView.isHidden = true
//        }
//        else if nextIndex == 1 {
//            let vc = self.orderedVC[1] as! SwipeVC
//            vc.swipeView.isHidden = false
//        }
        
        let orderedViewControllersCount = orderedVC.count
        
        guard orderedViewControllersCount != nextIndex else { return nil }
        
        guard orderedViewControllersCount > nextIndex else { return nil }
        print("*****after next vc: ", orderedVC[nextIndex])
        //        if orderedVC[nextIndex].isEqual(orderedVC[0]) {
        //            categoryBool = false
        //        }
        return orderedVC[nextIndex]
    }
    
}

extension SwipeControlVC: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            guard let viewController = pageViewController.viewControllers?.first,
                let index = orderedVC.index(of: viewController) else {
                    fatalError("Can't prevent bounce if there's not an index")
            }
//            currentIndex = index
//            self.webUrl(index: index)
        } else {
            print("didFinishAnimating")
        }
    }
    
    //    func pageViewController(_ pageViewController: UIPageViewController, spineLocationFor orientation: UIInterfaceOrientation) -> UIPageViewControllerSpineLocation {
    //        return UIPageViewControllerSpineLocation(rawValue: 2)!
    //    }
}
