//
//  SwipeCollectionVC.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 15/10/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit

private let reuseIdentifier = "SwipeCell"
var sourceUrl = ""

class SwipeCollectionVC: UICollectionViewController {

    var news = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register cell classes
        
        self.collectionView!.register(UINib(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        self.setup()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("view will disappeared")
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("view did disappeared")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setup() {
//        self.DBfetchNews()
        self.GetMultiWordNewsFeed()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.displayWebview(object:)), name: NSNotification.Name(rawValue: "displayWebview"), object: nil)
    }
    
    
    func DBfetchNews() {
        self.news = DatabaseNewsModel.fetchNews()
        if self.news.count <= 0 {
            self.GetMultiWordNewsFeed()
        } else {
            self.collectionView?.reloadData()
        }
    }
    
    func GetMultiWordNewsFeed() {
        API.getApi(controller: self, method: API.GetMultiWordNewsFeed, param: [:]) { (result) in
            guard result != nil else { return }
            if (result?["success"] as! String) == "1" {
                if let newsList = (result?["NewsList"]) {
                    (newsList as! NSArray).forEach({ (news) in
                        let n = news as! NSDictionary
                        DatabaseNewsModel.insertNews(dict: n)
                    })
                    self.DBfetchNews()
                }
            }
        }
    }

    @objc func displayWebview(object: Notification) {
        let currentNews = self.news[object.object as! Int] as! NewsTableModel
//            NewsModel(o: self.news[object.object as! Int] as! NSDictionary)
        sourceUrl = currentNews.source_url
        let vc =  SegueController.RightVCWebView()
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle   = .crossDissolve
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.news.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SwipeCell
        
        cell.btnSourceNTime.tag = indexPath.row
        cell.updateCell(dict: self.news[indexPath.row] as! NewsTableModel)
        
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionView?.cellForItem(at: indexPath) as! SwipeCell
        cell.showHideBottom(boolean: cell.contraintHeightTabbar.constant == BottomConstant.hide.rawValue ? false : true)
    }
}
