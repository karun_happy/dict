	//
//  RightVC.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 15/10/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit

class RightVC: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("webview did appaer")
        let url = URLRequest(url: URL(string: sourceUrl)!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 5)
        webView.loadRequest(url)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("webview will appaer")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let url = URLRequest(url: URL(string: "about:blank")!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 5)
        webView.loadRequest(url)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
