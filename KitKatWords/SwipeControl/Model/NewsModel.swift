//
//  NewsModel.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 26/10/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import Foundation

struct NewsModel {
    var dict = NSDictionary()
    init(o: NSDictionary) {
        dict = o
    }
    init() {}
    
    var Author: String {
        return GetSet.string(val: dict["Author"])
    }
    
    var Category: String {
        return GetSet.string(val: dict["Category"])
    }
    
    var Description: String {
        return GetSet.string(val: dict["Description"])
    }
    
    var Heading: String {
        return GetSet.string(val: dict["Heading"])
    }
    
    var HelpDesc: String {
        return GetSet.string(val: dict["HelpDesc"])
    }
    
    var HelpHead: String {
        return GetSet.string(val: dict["HelpHead"])
    }
    
    var Id: Int {
        return GetSet.integer(val: dict["Id"])
    }
    
    var ImgUrl: String {
        return GetSet.string(val: dict["ImgUrl"])
    }
    
    var IsPublish: String {
        return GetSet.string(val: dict["IsPublish"])
    }
    
    var IsShowHelpMsg: String {
        return GetSet.string(val: dict["IsShowHelpMsg"])
    }
    
    var IsShowSaveMsg: String {
        return GetSet.string(val: dict["IsShowSaveMsg"])
    }
    
    var Notify: String {
        return GetSet.string(val: dict["Notify"])
    }
    
    var Priority: Int {
        return GetSet.integer(val: dict["Priority"])
    }
    
    var PublishDate: String {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = ""
        let dd = GetSet.string(val: dict["PublishDate"])
        let d = dd.replacingOccurrences(of: "/Date(", with: "")
        let datetime = d.replacingOccurrences(of: ")/", with: "")
        let dt = Date(timeIntervalSince1970: TimeInterval.init(exactly: Double(datetime)!)!)
        
        return relativePast(date: dt)
//        return GetSet.string(val: dict["PublishDate"])
    }
    
    var SaveDesc: String {
        return GetSet.string(val: dict["SaveDesc"])
    }
    
    var SaveHead: String {
        return GetSet.string(val: dict["SaveHead"])
    }
    
    var SourceName: String {
        
        return GetSet.string(val: dict["SourceName"]) + " / " + PublishDate
    }
    
    var SourceUrl: String {
        return GetSet.string(val: dict["SourceUrl"])
    }

    var newsWord: NSArray {
        return GetSet.array(val: dict["NewsWordModels"])
    }
    
    struct NewsWordModel {
        var dict = NSDictionary()
        init(o: NSDictionary) {
            dict = o
        }
        init() {}
        var StartIndex: Int {
            return GetSet.integer(val: dict["StartIndex"])
        }
        
        var EndIndex: Int {
            return GetSet.integer(val: dict["EndIndex"])
        }
        
        var WordId: Int {
            return GetSet.integer(val: dict["WordId"])
        }
        
        var HindiMeaning: String {
            return GetSet.string(val: dict["HindiMeaning"])
        }
        var Word: String {
            return GetSet.string(val: dict["Word"])
        }
        
        var EngDef: String {
            return GetSet.string(val: dict["EngDef"])
        }
    }
    
    /// Convert Date Time into Year, month, day
    func relativePast(date dateString : Date) -> String {
        let units = Set<Calendar.Component>([.year, .month, .day, .hour, .minute, .second, .weekOfYear])
        let components = Calendar.current.dateComponents(units, from: dateString as Date, to: Date())
        
        var output = ""
        
        if components.year! > 0 {
            output = "\(components.year!) " + (components.year! > 1 ? "years ago" : "year ago")
            
        } else if components.month! > 0 {
            output = "\(components.month!) " + (components.month! > 1 ? "months ago" : "month ago")
            
        } else if components.weekOfYear! > 0 {
            output = "\(components.weekOfYear!) " + (components.weekOfYear! > 1 ? "weeks ago" : "week ago")
            
        } else if (components.day! > 0) {
            output = (components.day! > 1 ? "\(components.day!) days ago" : "Yesterday")
            
        } else if components.hour! > 0 {
            output = (components.hour! > 1 ? "Today" : "Today") //"\(components.hour!) " + (components.hour! > 1 ? "Today" : "Today")
            
        } else if components.minute! > 0 {
            output = (components.minute! > 1 ? "Today" : "Today") //"\(components.minute!) " + (components.minute! > 1 ? "Today" : "Today")
            
        } else {
            output = (components.second! > 1 ? "Today" : "Today") //"\(components.second!) " + (components.second! > 1 ? "Today" : "Today")
        }
        
        return output
    }
}
