//
//  Common.swift
//  TestTask
//
//  Created by Karun Aggarwal on 31/08/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import AVFoundation

class Common: NSObject {
    
/// Singleton Object
    class var share: Common {
        struct Static {
            static let instance: Common = Common()
        }
        return Static.instance
    }
    
/// Common Variables
    let screenSize = UIScreen.main.bounds
    let appName = "KitKatWords"
    let appColor = UIColor.colorFromCode(0x1B5AD5)
    let DBName = "KitKatWordsDB.sqlite"
    
/// Activity Indicator
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    let synth = AVSpeechSynthesizer()
    var utterance = AVSpeechUtterance()
    
/// Load Images
    func loadImage(img: UIImageView, url: URL) {
        img.kf.setImage(with: url)
    }
    
/// Speak Word
    func speakWord(title: String) {
        self.utterance = AVSpeechUtterance(string: title)
        self.utterance.rate = 0.45
        self.synth.speak(self.utterance)
    }
    
    func backLabel(label: UILabel, view: UIView, text: String) {
        label.frame = CGRect(x: 0, y: 0, width: self.screenSize.width, height: view.bounds.height)
        label.backgroundColor = UIColor.colorFromCode(0xf2f2f2)
        label.text = "No " + text + " for this word."
        label.textAlignment = .center
        view.addSubview(label)
    }
    
/// Loader - Start Activity Indicator
    func showActivityIndicator(_ uiView: UIView) {
        self.container.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.container.backgroundColor = UIColor(red: 18/255, green: 6/255, blue: 0/255, alpha: 0.3)
        
        self.loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = container.center
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        uiView.bringSubview(toFront: container)
        activityIndicator.startAnimating()
    }

/// Loader - Stop Activity Indicator
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
/// Alert Message
    func alertMessage(_ msg: String, action: Bool = true) -> UIAlertController {
        let alert = UIAlertController(title: self.appName, message: msg, preferredStyle: .alert)
        if action {
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
        }
        return alert
    }
    
/// Evaluate String Width
    func evaluateStringWidth(_ textToEvaluate: String, fontSize: CGFloat) -> CGFloat {
        let font = UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.regular)
        let attributes = NSDictionary(object: font, forKey: NSAttributedStringKey.font as NSCopying)
        let sizeOfText = textToEvaluate.size(withAttributes: (attributes as! [NSAttributedStringKey : Any]))
        
        return sizeOfText.width
    }
    
/// Table View Cell Back Color
    func cellBackColor(index: Int) -> UIColor {
        return ((index % 2) == 0) ? UIColor.colorFromCode(0xf5f5f5) : UIColor.white
    }
    
    func headerFontColor(label: UILabel, cell: Bool = false) {
        var color = UIColor()
        var font  = UIFont()
        if cell {
            color = UIColor.colorFromCode(0x3949f7)
            font  = UIFont.systemFont(ofSize: 20)
        } else {
            color = UIColor.colorFromCode(0xde4949)
            font  = UIFont.systemFont(ofSize: 14)
        }
        label.textColor = color
        label.font = font
    }
    
/// height according to string
    func getHeight(messageString: String) -> CGFloat{
        let attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 22.0)]
        
        let attributedString : NSAttributedString = NSAttributedString(string: messageString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, attributes: attributes)
        
        let rect : CGRect = attributedString.boundingRect(with: CGSize(width: Common.share.screenSize.width - 16, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        return (rect.height)
    }
    
    
    func snapshot(view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
        
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIColor {
    public static func colorFromCode(_ code: Int) -> UIColor {
        let red = CGFloat(((code & 0xFF0000) >> 16)) / 255
        let green = CGFloat(((code & 0xFF00) >> 8)) / 255
        let blue = CGFloat((code & 0xFF)) / 255
        
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
}
extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var bordersColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
