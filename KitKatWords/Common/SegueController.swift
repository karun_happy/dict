//
//  SegueController.swift
//  Golf Club
//
//  Created by Karun Aggarwal on 17/05/17.
//  Copyright © 2017 SquareLoops. All rights reserved.
//

import UIKit

class SegueController: NSObject {
    
/// Storyboard & Storu=yboard IDs
    class func storyboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    class func getController(str: String) -> UIViewController {
        return storyboard().instantiateViewController(withIdentifier: str + "ID")
    }
    
    class func SwipeStoryboard(str: String) -> UIViewController {
        return UIStoryboard(name: "Swipe", bundle: nil).instantiateViewController(withIdentifier: str + "ID")
    }

/// Pop View Controller
    class func popController(controller: UIViewController) {
        _ = controller.navigationController?.popViewController(animated: true)
    }
    
/// View Controller
    class func MainNavigationVC() -> MainNavigationVC {
        return getController(str: "MainNavigationVC") as! MainNavigationVC
    }
    
    class func ViewControllerVC() -> ViewController {
        return getController(str: "ViewController") as! ViewController
    }
    
    class func SearchVC() -> SearchVC {
        return getController(str: "SearchVC") as! SearchVC
    }
    
    class func DictionaryVC() -> DictionaryVC {
        return getController(str: "DictionaryVC") as! DictionaryVC
    }
    
    class func DefinitionTableVC() -> DefinitionTableVC {
        return getController(str: "DefinitionTableVC") as! DefinitionTableVC
    }
    
    class func SentencesTableVC() -> SentencesTableVC {
        return getController(str: "SentencesTableVC") as! SentencesTableVC
    }
    
    class func SynonymsTableVC() -> SynonymsTableVC {
        return getController(str: "SynonymsTableVC") as! SynonymsTableVC
    }
    
    class func AntonymsTableVC() -> AntonymsTableVC {
        return getController(str: "AntonymsTableVC") as! AntonymsTableVC
    }
    
    class func RightVCWebView() -> RightVC {
        return SwipeStoryboard(str: "RightVC") as! RightVC
    }
}
