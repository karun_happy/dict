//
//  DictionaryModel.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 14/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import Foundation

class GetSet: NSObject {
    /// Validate data & return as String
    static func string(val: Any?) -> String {
//        guard (val != nil) else { return "" }
//        if val != nil {
//            if let notNull =  val as? String {
//                return notNull
//            } else {
//                return ""
//            }
//        } else {
//            return ""
//        }
        return val as? String ?? ""
    }
    
    /// Validate data & return as Integer
    static func integer(val: Any?) -> Int {
        var v = 0
        v = val as? Int ?? 0
        return v
    }
    
    /// Validate data & return as Array
    static func array(val: Any?) -> NSArray {
        var v = NSArray()
        v = val as? NSArray ?? []
        return v
    }
    
/// Validate data & return as Dictionary
    static func dictionary(val: Any?) -> NSDictionary {
        var v = NSDictionary()
        v = (val as? NSDictionary ?? nil)!
        return v
    }
}
/*
 public enum Grammar: Int
 {
    case Noun = 1
    case Verb = 2
    case Adjective = 3
    case Adverb = 4
    case TransitiveVerb = 5
    case Preposition = 6
    case Phrase = 7
    case Pronoun = 8
    case Interjection = 9
    case IntransitiveVerb = 10
    case Conjunction = 11
    case PhrasalVerb = 12
    case Prefix = 13
    case Idioms = 14
    case Article = 16
    case Abbreviation = 17
    case Determiner = 18
    case Preferences = 19
    case TransitivePhrasalVerb = 21
    case RelativePronoun = 22
    case IntransitivePhrasalVerb = 23
    case AuxuliaryVerb = 24
    case CombiningForm = 25
    case Particle = 26
    case Suffix = 27
    case Interrogative = 28
    case VerbPhrase = 29
    case NounAndAdjective = 30
    case ModelVerb = 31
    case ReflexivePronoun = 32
    case PronounAndDeterminer = 33
    case ProperName = 34
    case NoundAndInterjection = 35
    case NounAndPronoun = 36
    case Unknown = 37
    case NoundAndDeterminer = 38
}
*/
public func getTitle(num: Int) -> String {
    var val = ""
    switch num {
    case 1:
        val = "Noun"
        break
    case 2:
        val = "Verb"
        break
    case 3:
        val = "Adjective"
        break
    case 4:
        val = "Adverb"
        break
    case 5:
        val = "TransitiveVerb"
        break
    case 6:
        val = "Preposition"
        break
    case 7:
        val = "Phrase"
        break
    case 8:
        val = "Pronoun"
        break
    case 9:
        val = "Interjection"
        break
    case 10:
        val = "IntransitiveVerb"
        break
    case 11:
        val = "Conjunction"
        break
    case 12:
        val = "PhrasalVerb"
        break
    case 13:
        val = "Prefix"
        break
    case 14:
        val = "Conjunction"
        break
    case 16:
        val = "Idioms"
        break
    case 17:
        val = "Abbreviation"
        break
    case 18:
        val = "Determiner"
        break
    case 19:
        val = "Preferences"
        break
    case 21:
        val = "TransitivePhrasalVerb"
        break
    case 22:
        val = "RelativePronoun"
        break
    case 23:
        val = "IntransitivePhrasalVerb"
        break
    case 24:
        val = "AuxuliaryVerb"
        break
    case 25:
        val = "CombiningForm"
        break
    case 25:
        val = "Particle"
        break
    case 25:
        val = "Suffix"
        break
    case 25:
        val = "Interrogative"
        break
    case 29:
        val = "VerbPhrase"
        break
    case 30:
        val = "NounAndAdjective"
        break
    case 31:
        val = "ModelVerb"
        break
    case 32:
        val = "ReflexivePronoun"
        break
    case 33:
        val = "PronounAndDeterminer"
        break
    case 34:
        val = "ProperName"
        break
    case 35:
        val = "NoundAndInterjection"
        break
    case 36:
        val = "NounAndPronoun"
        break
    case 37:
        val = "Unknown"
        break
    case 38:
        val = "NoundAndDeterminer"
        break
    default:
        break
    }
    return val
}


struct DictionaryModel {
    var dict = NSDictionary()
    init(o: NSDictionary) {
        dict = o
    }
    init() {}
    
    var data: NSDictionary {
        return dict["data"] as? NSDictionary ?? [:]
    }
}

struct Data {
    var datadict = NSDictionary()
    init(o: NSDictionary) {
        datadict = o
    }
    var BucketId: Int {
        return GetSet.integer(val: datadict.value(forKey: "BucketId"))
    }
    
    var WordId: Int {
        return GetSet.integer(val: datadict.value(forKey: "WordId"))
    }
    
    var LemmaId: Int {
        return GetSet.integer(val: datadict.value(forKey: "LemmaId"))
    }
    
    var Word: String {
        return GetSet.string(val: datadict.value(forKey: "Word"))
    }
    
    var Meanings: NSArray {
        return GetSet.array(val: datadict.value(forKey: "Meanings"))
    }
    
    var Definition: NSArray {
        return GetSet.array(val: datadict.value(forKey: "Definition"))
    }
    
    var Antonyms: NSArray {
        return GetSet.array(val: datadict.value(forKey: "Antonyms"))
    }
    
    var Synonyms: NSArray {
        return GetSet.array(val: datadict.value(forKey: "Synonyms"))
    }
}

struct Meaning {
    var dict = NSDictionary()
    init(o: NSDictionary) {
        dict = o
    }
    init() {}
    
    var MId: Int {
        return GetSet.integer(val: dict.value(forKey: "MId"))
    }
    
    var G: Int {
        return GetSet.integer(val: dict.value(forKey: "G"))
    }
    
    var F: Int {
        return GetSet.integer(val: dict.value(forKey: "F"))
    }
    
    var M: String {
        return GetSet.string(val: dict.value(forKey: "M"))
    }
}

struct Definition {
    var dict = NSDictionary()
    init(o: NSDictionary) {
        dict = o
    }
    init() {}
    
    var Sd: Int {
        return GetSet.integer(val: dict.value(forKey: "Sd"))
    }
    
    var DId: Int {
        return GetSet.integer(val: dict.value(forKey: "DId"))
    }
    
    var G: Int {
        return GetSet.integer(val: dict.value(forKey: "G"))
    }
    
    var M: String {
        return GetSet.string(val: dict.value(forKey: "M"))
    }
    
    var F: Int {
        return GetSet.integer(val: dict.value(forKey: "F"))
    }
}
