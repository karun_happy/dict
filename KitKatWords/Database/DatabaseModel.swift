//
//  DatabaseModel.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 18/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import Foundation

class SearchSuggestion: NSObject {
    var word_id: Int = Int()
    var word: String = String()
    var lem_id: Int  = Int()
    var mode: Int    = Int()
    var character_type: String = String()
}

class SearchHistory: NSObject {
    var word: String = String()
    var word_id: Int = Int()
    var lem_word: String = String()
    var count: Int = 0
    var searched_at: String = String()
    var synced: Int  = 0
    var deleted: Int = 0
}

let shared = ModelManager()
class ModelManager: NSObject {
    
    var database: FMDatabase? = nil
    
    class func getInstance() -> ModelManager {
        if shared.database == nil {
            shared.database = FMDatabase(path: Utility.getPath(fileName: Common.share.DBName))
        }
        return shared
    }
    
//*********************** History Queries ***********************
//MARK: - HISTORY Queries
    func addHistory(history: SearchHistory) -> Bool {
        shared.database?.open()
        let isInserted = shared.database?.executeUpdate(
            "INSERT INTO search_history (word,word_id,lem_word, searched_at, count) VALUES (?,?,?,?,?)",
            withArgumentsIn: [history.word, history.word_id, history.lem_word, history.searched_at, history.count])
        shared.database?.close()
        return isInserted!
    }
    
    func getHistory(arg: Bool = false, id: Int = 0) -> NSMutableArray {
        shared.database?.open()
        
        let qry = arg ? "SELECT * from search_history where word_id = ?" : "select * from search_history order by searched_at DESC"
        
        let resultSet: FMResultSet = (shared.database?.executeQuery(
            qry,
            withArgumentsIn: (arg ? [id] : nil)))!
        let history = NSMutableArray()

            while resultSet.next() {
                let historyInfo = SearchHistory()
                historyInfo.word     = resultSet.string(forColumn: "word")
                historyInfo.word_id  = Int(resultSet.int(forColumn: "word_id"))
                historyInfo.lem_word = resultSet.string(forColumn: "lem_word")
                historyInfo.count    = Int(resultSet.int(forColumn: "count"))
                historyInfo.lem_word = resultSet.string(forColumn: "lem_word")
                historyInfo.searched_at = resultSet.string(forColumn: "searched_at")
                historyInfo.synced  = Int(resultSet.int(forColumn: "synced"))
                historyInfo.deleted = Int(resultSet.int(forColumn: "deleted"))
                history.add(historyInfo)
            }
        return history
    }
    
    func updateHistory(history: SearchHistory) -> Bool {
        shared.database?.open()
        let isInserted = shared.database?.executeUpdate(
            "UPDATE search_history SET count = ?, searched_at = ? WHERE word_id = ?",
            withArgumentsIn: [history.count, history.searched_at, history.word_id])
        shared.database?.close()
        return isInserted!
    }
    
//*********************** Search Suggestions Queries ***********************
//MARK: - SEARCH SUGGESTIONS QUERIES
    func filterSearch(character: String) -> NSMutableArray {
        shared.database?.open()
        
        let qry = "SELECT * from search_suggestion WHERE word LIKE ? LIMIT 30"
        
        let resultSet: FMResultSet = (shared.database?.executeQuery(qry, withArgumentsIn: [String(format: "%@%%", character)]))!
        let search = NSMutableArray()
        
        while resultSet.next() {
            let suggestionInfo = SearchSuggestion()
            suggestionInfo.word    = resultSet.string(forColumn: "word")
            suggestionInfo.word_id = Int(resultSet.string(forColumn: "word_id"))!
            suggestionInfo.lem_id  = Int(resultSet.string(forColumn: "lem_id"))!
            suggestionInfo.mode    = Int(resultSet.string(forColumn: "mode"))!
            suggestionInfo.character_type = resultSet.string(forColumn: "character_type")
            search.add(suggestionInfo)
        }
        
        return search
    }
    
    func addSuggestions(history: SearchSuggestion) -> Bool {
        shared.database?.open()
        let isInserted = shared.database?.executeUpdate(
            "INSERT INTO search_suggestion (word,word_id,lem_id, character_type, mode) VALUES (?,?,?,?,?)",
            withArgumentsIn: [history.word, history.word_id, history.lem_id, history.character_type, history.mode])
        shared.database?.close()
        return isInserted!
    }
    
//*********************** Synonym Queries ***********************
//MARK: - Synonym Queries
    func getSynonym(syn: NSArray) -> NSMutableArray {
        shared.database?.open()
        let qry = String(format: "select * from search_suggestion where word_id IN (%@)", syn.componentsJoined(by: ","))
        let resultSet: FMResultSet = (shared.database?.executeQuery(qry, withArgumentsIn: nil))!

        let synonym = NSMutableArray()
        while resultSet.next() {
            synonym.add(resultSet.string(forColumn: "word").description)
        }
        return synonym
    }

//*********************** Antonym Queries ***********************
//MARK: - Antonym Queries
    func getAntonym(antonym: NSArray) -> NSMutableArray {
        shared.database?.open()
        let qry = String(format: "select * from search_suggestion where word_id IN (%@)", antonym.componentsJoined(by: ","))
        let resultSet: FMResultSet = (shared.database?.executeQuery(qry, withArgumentsIn: nil))!
        
        let synonym = NSMutableArray()
        while resultSet.next() {
            synonym.add(resultSet.string(forColumn: "word").description)
        }
        return synonym
    }
}
