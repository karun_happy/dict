//
//  DatabaseNewsModel.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 31/10/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import Foundation

class NewsTableModel: NSObject {
    var id = Int()
    var hash_id = Int()
    var category_id = Int()
    var show_save_word_message = Int()
    var show_share_news_message = Int()
    var priority = Int()
    var word_id  = Int()
    var created_at = 0
    var score = 0
    var read  = 0
    var title = String()
    var content = String()
    var author_name = String()
    var image_url   = String()
    var source_name = String()
    var source_url  = String()
    var category    = String()
    var save_word_title = String()
    var save_word_description = String()
    var share_news_title = String()
    var share_news_description = String()
    var word = String()
    var meaning = String()
    var def = String()
    var s_index = -1
    var E_index = -1
//    var created_at INT DEFAULT 0 ,title TEXT,content TEXT,author_name TEXT,image_url TEXT,source_name TEXT,source_url TEXT,score INT DEFAULT 0 ,read INT DEFAULT 0 ,category TEXT,category_id INT,save_word_title TEXT,save_word_description TEXT,show_save_word_message INT,show_share_news_message INT,share_news_title TEXT,share_news_description TEXT,priority INT,word_id INT,word TEXT,meaning TEXT,def TEXT,s_index INT DEFAULT -1 ,E_index INT DEFAULT -1
}

class DatabaseNewsModel {
//*********************** News Related Queries ***********************
    public static let db = ModelManager.getInstance().database
    
/// CREATE NEWS TABLE
    public static func createNewsTable() {
        DatabaseNewsModel.db?.open()
        let qry = SQL_CREATE_TABLE_NEWS
        let q = DatabaseNewsModel.db?.executeUpdate(qry, withArgumentsIn: nil)
        print(q!.description)
        print("News table added to database")
        DatabaseNewsModel.db?.close()
    }
    
/// INSERT NEWS INTO TABLE
    public static func insertNews(dict: NSDictionary) {
        let news = NewsModel(o: dict)
        let wordModel = NewsModel.NewsWordModel(o: news.newsWord[0] as! NSDictionary)

    /// News already there in the database
        if DatabaseNewsModel.fetchNews(arg: false, id: news.Id).count > 0 {
            return
        }

        DatabaseNewsModel.db?.open()
        let qry = "INSERT INTO ns (id,hash_id,created_at,title,content,author_name,image_url,source_name,source_url,score,read,category,category_id,save_word_title,save_word_description,show_save_word_message,show_share_news_message,share_news_title,share_news_description,priority,word_id,word,meaning,def,s_index,E_index) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

        let added = DatabaseNewsModel.db?.executeUpdate(qry, withArgumentsIn:
            [news.Id,
             news.Id,
             Int(NSDate().timeIntervalSince1970 * 1000),
             news.Heading,
             news.Description,
             news.Author,
             news.ImgUrl,
             news.SourceName,
             news.SourceUrl,
             0,
             0,
             "category",
             1,
             "Save Word Title",
             "Save Word Description",
             1,
             "Show Save Message",
             news.Heading,
             news.Description,
             news.Priority,
             wordModel.WordId,
             wordModel.Word,
             wordModel.HindiMeaning,
             wordModel.EngDef,
             -1,
             -1
            ]
        )
        DatabaseNewsModel.db?.close()
        
        print("News item added status ", added!.description)
    }
    
/// FETCH NEWS
    public static func fetchNews(arg: Bool = true, id: Int = 0) -> NSMutableArray {
        DatabaseNewsModel.db?.open()
        
        let qry = arg ? "SELECT * from ns" : "SELECT * from ns where id = ? "
        let result: FMResultSet = (DatabaseNewsModel.db?.executeQuery(qry, withArgumentsIn: arg ? nil : [id]))!
        let news = NSMutableArray()
        
        while result.next() {
            let n = NewsTableModel()
            n.title   = result.string(forColumn: "title")
            n.content = result.string(forColumn: "content")
            n.source_name = result.string(forColumn: "source_name")
            n.source_url  = result.string(forColumn: "source_url")
            n.image_url   = result.string(forColumn: "image_url")
            news.add(n)
        }
        DatabaseNewsModel.db?.close()
        return news
    }
}


public var SQL_CREATE_TABLE_NEWS: String {
    let COMMA_SEP = ","
    let INTEGER_TYPE = " INT"
    let TEXT_TYPE = " TEXT"
    
    return "CREATE TABLE IF NOT EXISTS "
        + BaseColumns.TABLE_NAME + "("
        + BaseColumns._ID + INTEGER_TYPE + " PRIMARY KEY NOT NULL " + COMMA_SEP
        + BaseColumns.COLUMN_NAME_HASH_ID + INTEGER_TYPE + " UNIQUE " + COMMA_SEP
        + BaseColumns.COLUMN_NAME_CREATED_AT + INTEGER_TYPE + " DEFAULT 0 " + COMMA_SEP
        + BaseColumns.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_CONTENT + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_AUTHOR_NAME + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_IMAGE_URL + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_SOURCE_NAME + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_SOURCE_URL + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_SCORE + INTEGER_TYPE + " DEFAULT 0 " + COMMA_SEP
        + BaseColumns.COLUMN_NAME_READ + INTEGER_TYPE + " DEFAULT 0 " + COMMA_SEP
        + BaseColumns.COLUMN_NAME_CATEGORY + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_CATEGORY_ID + INTEGER_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_SAVE_WORD_MESSAGE_TITLE + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_SAVE_WORD_MESSAGE_CONTENT + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_SHOW_SAVE_WORD_MESSAGE + INTEGER_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_SHOW_SHARE_NEWS_MESSAGE + INTEGER_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_SHARE_NEWS_TITLE + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_SHARE_NEWS_CONTENT + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_PRIORITY + INTEGER_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_WORD_ID + INTEGER_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_WORD + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_MEANING + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_DEFINITION + TEXT_TYPE + COMMA_SEP
        + BaseColumns.COLUMN_NAME_WORD_START_INDEX + INTEGER_TYPE + " DEFAULT -1 " + COMMA_SEP
        + BaseColumns.COLUMN_NAME_WORD_End_INDEX + INTEGER_TYPE +  " DEFAULT -1 "
        + ")"
}

class BaseColumns {
    
    public static let TABLE_NAME = "ns";
    public static let _ID = "id";
    public static let COLUMN_NAME_HASH_ID = "hash_id";
    public static let COLUMN_NAME_TITLE = "title";
    public static let COLUMN_NAME_CONTENT = "content";
    public static let COLUMN_NAME_CREATED_AT = "created_at";
    public static let COLUMN_NAME_AUTHOR_NAME = "author_name";
    public static let COLUMN_NAME_IMAGE_URL = "image_url";
    public static let COLUMN_NAME_SOURCE_NAME = "source_name";
    public static let COLUMN_NAME_SOURCE_URL = "source_url";
    public static let COLUMN_NAME_READ = "read";
    public static let COLUMN_NAME_SCORE = "score";
    
    public static let COLUMN_NAME_CATEGORY = "category";
    public static let COLUMN_NAME_CATEGORY_ID = "category_id";
    public static let COLUMN_NAME_PRIORITY = "priority";
    public static let COLUMN_NAME_SHOW_SHARE_NEWS_MESSAGE = "show_share_news_message";
    public static let COLUMN_NAME_SHARE_NEWS_TITLE = "share_news_title";
    public static let COLUMN_NAME_SHARE_NEWS_CONTENT = "share_news_description";
    public static let COLUMN_NAME_SHOW_SAVE_WORD_MESSAGE = "show_save_word_message";
    public static let COLUMN_NAME_SAVE_WORD_MESSAGE_TITLE = "save_word_title";
    public static let COLUMN_NAME_SAVE_WORD_MESSAGE_CONTENT = "save_word_description";
    
    public static let COLUMN_NAME_WORD_ID = "word_id";
    public static let COLUMN_NAME_WORD = "word";
    public static let COLUMN_NAME_DEFINITION = "def";
    public static let COLUMN_NAME_MEANING = "meaning";
    public static let COLUMN_NAME_WORD_START_INDEX = "s_index";
    public static let COLUMN_NAME_WORD_End_INDEX = "E_index";
}
