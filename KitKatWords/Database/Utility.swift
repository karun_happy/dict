//
//  Utility.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 18/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import Foundation

class Utility: NSObject {
    
    class func getPath(fileName: String) -> String {
        let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let fileURL = documentURL[documentURL.endIndex - 1].appendingPathComponent(fileName)
        return fileURL.path
    }
    
    class func copyFile(fileName: NSString) {
        let dbPath: String = getPath(fileName: fileName as String)
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: dbPath) {
            let documentURL = Bundle.main.resourceURL
            let fromPath    = documentURL?.appendingPathComponent(fileName as String)
            var error: NSError?
            
            do {
                try fileManager.copyItem(atPath: (fromPath?.path)!, toPath: dbPath)
            } catch let err as NSError {
                error = err
            }
            let alert: UIAlertView = UIAlertView()
            if error != nil {
                alert.title = "Error Occured"
                alert.message = error?.localizedDescription
            } else {
                alert.title = "Successfully copied"
                alert.message = "Your databse copy succesfully"
                
            /// Insert News Table
                DatabaseNewsModel.createNewsTable()
            }
            print(alert.message!)
//            alert.delegate = nil
//            alert.addButton(withTitle: "OK")
//            alert.show()
        }
    }
    
    class func invokeAlertMethod(strTitle: NSString, strBody: NSString, delegate: AnyObject?)
    {
        let alert: UIAlertView = UIAlertView()
        alert.message = strBody as String
        alert.title = strTitle as String
        alert.delegate = delegate
        alert.addButton(withTitle: "Ok")
        alert.show()
    }
}
