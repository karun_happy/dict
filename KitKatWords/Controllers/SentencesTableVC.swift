//
//  SentencesTableVC.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 15/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit

class SentencesTableVC: UITableViewController {

    var arraySentences = NSArray()
    var noLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Common.share.backLabel(label: self.noLabel, view: self.view, text: "sentence")
    }

    func getSenetence(wordid: Int) {
        API.getApi(controller: self, method: API.sentence + wordid.description, param: [:]) { (result) in
            guard result != nil else { return }
            if (result?["success"] as! String) == "1" {
                let data = result?["data"] as? NSArray ?? []
                self.arraySentences = data
                if self.arraySentences.count <= 0 {
                    self.noLabel.isHidden = false
                    self.view.bringSubview(toFront: self.noLabel)
                } else {
                    self.noLabel.isHidden = true
                }
            } else {
                self.noLabel.isHidden = false
                self.view.bringSubview(toFront: self.noLabel)
            }
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.arraySentences.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SentenceTableCell", for: indexPath) as! SentenceTableCell

        cell.updateCell(dict: self.arraySentences[indexPath.row] as! NSDictionary)
        return cell
    }
 
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Common.share.cellBackColor(index: indexPath.row)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
