//
//  AntonymsTableVC.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 15/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit
import AVFoundation

class AntonymsTableVC: UITableViewController {

    var arrayAntonym = NSArray()
    var noLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Common.share.backLabel(label: self.noLabel, view: self.view, text: "antonym")
    }

    func updateTable(Antonym: NSArray) {
        self.arrayAntonym = ModelManager().getAntonym(antonym: Antonym)
        print("Antonym array: ")
        print(self.arrayAntonym)
        if self.arrayAntonym.count <= 0 {
            self.noLabel.isHidden = false
            self.view.bringSubview(toFront: self.noLabel)
        } else {
            self.noLabel.isHidden = true
        }
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.arrayAntonym.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AntonymsTableCell", for: indexPath)
        cell.textLabel?.text = self.arrayAntonym[indexPath.row] as? String
        
        cell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "headphones"))
        cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        
        Common.share.headerFontColor(label: cell.textLabel!, cell: true)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Common.share.cellBackColor(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Common.share.speakWord(title: self.arrayAntonym[indexPath.row] as! String)
    }
}
