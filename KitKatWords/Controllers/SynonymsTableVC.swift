//
//  SynonymsTableVC.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 15/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit
import AVFoundation

class SynonymsTableVC: UITableViewController {

    var arraySynonym = NSArray()
    var noLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Common.share.backLabel(label: self.noLabel, view: self.view, text: "synonym")
    }

    func updateTable(synonym: NSArray) {
        self.arraySynonym = ModelManager().getSynonym(syn: synonym)
        print("Synonym array: ")
        print(self.arraySynonym)
        if self.arraySynonym.count <= 0 {
            self.noLabel.isHidden = false
            self.view.bringSubview(toFront: self.noLabel)
        } else {
            self.noLabel.isHidden = true
        }
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.arraySynonym.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SynonymsTableCell", for: indexPath)
        cell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "headphones"))
        cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        cell.textLabel?.text = self.arraySynonym[indexPath.row] as? String
        
        Common.share.headerFontColor(label: cell.textLabel!, cell: true)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Common.share.cellBackColor(index: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Common.share.speakWord(title: self.arraySynonym[indexPath.row] as! String)
    }
}
