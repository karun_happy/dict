//
//  DictionaryVC.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 12/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit
import AVFoundation

class DictionaryVC: UIViewController {

    var noLabel = UILabel()
    var buttonTitle =  UIButton(type: .custom)
    
    @IBOutlet var topSegmentView: HMSegmentedControl!
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var viewMeaning: UIView!
    @IBOutlet var viewButtons: UIView!
    @IBOutlet var btnLearn: UIButton!
    @IBOutlet var btnWordSpeak: UIButton!
    @IBOutlet var tableViewMeanings: UITableView!
    @IBOutlet var viewImages: UIView!
    @IBOutlet var scrollViewImages: UIScrollView!

    var definitionVC = SegueController.DefinitionTableVC()
    var sentenceVC   = SegueController.SentencesTableVC()
    var synonymVC    = SegueController.SynonymsTableVC()
    var antonymVC    = SegueController.AntonymsTableVC()
    
    let synth = AVSpeechSynthesizer()
    var utterance = AVSpeechUtterance()
    
    var titleFont = CGFloat()
    var data = NSDictionary()
    var modelDictionary = DictionaryModel()
    var dataArray = [String: [Meaning]]()
    var searchedWord = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollView.frame.size.width = Common.share.screenSize.width
        self.scrollView.contentSize = CGSize(width: self.setXAxis(x: 5), height: self.scrollView.bounds.height)
//        self.scrollViewImages.contentSize = CGSize(width: (150 * 3), height: 150)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addControllers()
    }
    
    func setup() {
        self.segments()
        self.displayData()
        
        Common.share.backLabel(label: self.noLabel, view: self.scrollView, text: "meaning")
        
//        var titleView = self.navigationItem.titleView
        let button =  self.buttonTitle
        button.frame = CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: 40)
//        button.frame = CGRect(x: 0, y: 0, width: Common.share.getHeight(messageString: self.searchedWord), height: 40)
//        button.setTitle(self.searchedWord, for: .normal)
//        button.setTitleColor(UIColor.brown, for: .normal)
//        button.backgroundColor = UIColor.red.withAlphaComponent(0.5)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        button.titleLabel?.minimumScaleFactor = 0.8
        button.addTarget(self, action: #selector(self.titleAction), for: .touchUpInside)
        self.navigationItem.titleView = button
        
//        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.titleAction))
//        tapGesture.cancelsTouchesInView = false
//        self.navigationController?.view.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateView(object:)), name: NSNotification.Name(rawValue: "UpdateView"), object: nil)
    }
    
    func segments() {
        let control = self.topSegmentView
        control?.segmentWidthStyle = .dynamic
        control?.sectionTitles = ["MEANINGS", "DEFINITIONS", "SENTENCES", "SYNONYMS", "ANTONYMS"]
        control?.autoresizingMask = [.flexibleRightMargin, .flexibleWidth]
        control?.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10)
        control?.selectionStyle   = .fullWidthStripe
        control?.selectionIndicatorLocation = .down
        control?.isVerticalDividerEnabled = false
        control?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)]
        control?.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]
        control?.addTarget(self, action: #selector(segmentAction(_:)), for: .valueChanged)
        
        control?.selectionIndicatorColor  = Common().appColor
        control?.selectionIndicatorHeight = 5
        
        let bottomBorder = UIView(frame: CGRect(x: 0, y: ((control?.bounds.height)! - 1), width: Common.share.screenSize.width, height: 1))
        bottomBorder.backgroundColor = Common.share.appColor
        control?.addSubview(bottomBorder)
    }
    
    @objc func titleAction() {
        let search = SegueController.SearchVC()
        search.modalPresentationStyle = .overFullScreen
        search.modalTransitionStyle = .crossDissolve
        search.fromView = Views.Dict.rawValue
        search.viewBackground(image: Common.share.snapshot(view: self.view))
        search.txtSearch.text = self.searchedWord
        self.present(search, animated: true, completion: nil)
    }
    
    @objc func UpdateView(object: NSNotification) {
        self.topSegmentView.selectedSegmentIndex = 0
        self.scrollView.contentOffset.x = self.setXAxis(x: CGFloat(0))
        
        let obj = object.object as! NSArray

        self.data = obj[0] as! NSDictionary
        self.searchedWord = obj[1] as! String
        
        self.displayData()
    }
    
    func displayData() {
        modelDictionary = DictionaryModel(o: self.data)
        print(Common.share.getHeight(messageString: self.searchedWord))
        self.buttonTitle.frame.size.width = CGFloat.greatestFiniteMagnitude //Common.share.getHeight(messageString: self.searchedWord)
        self.buttonTitle.setTitle(self.searchedWord, for: .normal)
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: titleFont)]
        self.btnWordSpeak.setTitle(self.searchedWord + " ", for: .normal)
        displayImages()
        self.formatData()
        
        self.definitionVC.arrayDefinition = Data(o: modelDictionary.data).Definition
        self.definitionVC.formatData()
        self.sentenceVC.getSenetence(wordid: Data(o: modelDictionary.data).WordId)
        
        self.synonymVC.updateTable(synonym: Data(o: modelDictionary.data).Synonyms)
        
        self.antonymVC.updateTable(Antonym: Data(o: modelDictionary.data).Antonyms)
    }
    
    func formatData() {
        if Data(o: modelDictionary.data).Meanings.count <= 0 {
            self.noLabel.isHidden = false
            self.scrollView.bringSubview(toFront: self.noLabel)
            self.tableViewMeanings.reloadData()
            return
        }
        
        self.noLabel.isHidden = true
        var di = [String: [Meaning]]()
        Data(o: modelDictionary.data).Meanings.forEach { (element) in
            let elem = Meaning(o: element as! NSDictionary)
            let key = getTitle(num: elem.G)
            
            if di.index(forKey: key) != nil {
                di[key]?.append(elem)
            } else {
                di[key] = [elem]
            }
        }
        self.dataArray = di
        self.tableViewMeanings.reloadData()
    }
    
    func displayImages() {
        let viewWidth = (Common.share.screenSize.width / 3) - 7 //150
        self.scrollViewImages.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        for i in 0..<3 {
            let xValue = (viewWidth * CGFloat(i))
            let image = UIImageView(frame: CGRect(x: xValue + CGFloat(5 * (1 + i)), y: 0, width: viewWidth, height: viewWidth))
            image.contentMode = .scaleAspectFit
            let word  = i == 0 ? self.searchedWord : self.searchedWord + i.description
            let imageurl = "http://imgkitkatwords.com/imgwords/\(word).jpg".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            if (NSData(contentsOf: URL(string: imageurl!)!) != nil) {
                Common.share.loadImage(img: image, url: URL(string: imageurl!)!)
                self.scrollViewImages.addSubview(image)
            }
        }
    }
    
    func addControllers() {
        self.viewMeaning.frame.size.width = self.scrollView.bounds.width
        self.viewDefinition()
        self.viewSentence()
        self.viewSynonym()
        self.viewAntonym()
    }
    
    func setXAxis(x: CGFloat) -> CGFloat {
        return (self.scrollView.bounds.width * x)
    }
    
    func viewDefinition() {
        self.addViews(view: self.definitionVC.tableView, axis: 1)
    }
    
    func viewSentence() {
//        self.sentenceVC.getSenetence(wordid: Data(o: modelDictionary.data).WordId)
        self.addViews(view: self.sentenceVC.tableView, axis: 2)
    }
    
    func viewSynonym() {
        self.addViews(view: self.synonymVC.tableView, axis: 3)
    }
    
    func viewAntonym() {
        self.addViews(view: self.antonymVC.tableView, axis: 4)
    }
    
    func addViews(view: UITableView, axis: Int) {
        let viewWidth = self.view.frame.width
        let uiview = UIView()
        uiview.frame = CGRect(
            x: CGFloat(viewWidth * CGFloat(axis)),
            y: 0,
            width: viewWidth,
            height: self.viewMeaning.bounds.height + 70)
//          self.scrollView.backgroundColor = UIColor.cyan
//        view.frame = uiview.frame
        view.frame = CGRect(
            x: 0,
            y: 0,
            width: viewWidth,
            height: uiview.bounds.height)
        uiview.addSubview(view)
        self.scrollView.addSubview(uiview)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnBarAction(_ sender: UIBarButtonItem) {
        switch sender.tag {
        case 1:
    /// Back Button
            SegueController.popController(controller: self)
            break
        case 2:
    /// Search Button
            let search = SegueController.SearchVC()
            search.modalPresentationStyle = .overFullScreen
            search.modalTransitionStyle = .crossDissolve
            search.fromView = Views.Dict.rawValue
            search.viewBackground(image: Common.share.snapshot(view: self.view))
            self.present(search, animated: true, completion: nil)
            break
        default:
            break
        }
        
    }
    
    @objc func segmentAction(_ sender: HMSegmentedControl) {
        UIScrollView.animate(withDuration: 0.23, delay: 0.0, options: .curveEaseInOut, animations: { 
            self.scrollView.contentOffset.x = self.setXAxis(x: CGFloat(sender.selectedSegmentIndex))
        }, completion: nil)
    }
    
    @IBAction func btnWordSpeakAction(_ sender: UIButton) {
//        self.utterance = AVSpeechUtterance(string: )
//        self.utterance.rate = 0.45
//        self.synth.speak(self.utterance)
        Common.share.speakWord(title: sender.title(for: .normal)!)
    }
}
extension DictionaryVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.dataArray.keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("rows: ", Array(self.dataArray.keys)[section].count)
        print("rows section: ", Array(self.dataArray.keys)[section])
        let mean = Array(self.dataArray.keys)[section]
        return (self.dataArray[mean]?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DictionaryCell", for: indexPath)

        let mean = Array(self.dataArray.keys)[indexPath.section]
        let text = self.dataArray[mean]![indexPath.row]
        cell.textLabel?.text = text.M
        
        cell.indentationLevel = 1
//        cell.textLabel?.textColor = Common.share.appColor
        
        Common.share.headerFontColor(label: cell.textLabel!, cell: true)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            Common.share.headerFontColor(label: headerView.textLabel!)
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Array(self.dataArray.keys)[section]
    }
}
extension DictionaryVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Common.share.cellBackColor(index: indexPath.row)
    }
}
extension DictionaryVC: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = (scrollView.contentOffset.x / Common.share.screenSize.width)
        self.topSegmentView.setSelectedSegmentIndex(UInt(index), animated: true)
    }
}
