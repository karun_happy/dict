//
//  DefinitionTableVC.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 15/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit

class DefinitionTableVC: UITableViewController {

    var arrayDefinition = NSArray()
    var dataArray = [String: [Definition]]()
    
    var noLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Common.share.backLabel(label: self.noLabel, view: self.view, text: "definition")
        
        self.formatData()
    }

    func formatData() {
        if arrayDefinition.count <= 0 {
            self.noLabel.isHidden = false
            self.view.bringSubview(toFront: self.noLabel)
            self.tableView.reloadData()
         return
        }
        self.noLabel.isHidden = true
        var di = [String: [Definition]]()
        arrayDefinition.forEach { (element) in
            let elem = Definition(o: element as! NSDictionary)
            let key = getTitle(num: elem.G)
            
            if di.index(forKey: key) != nil {
                di[key]?.append(elem)
            } else {
                di[key] = [elem]
            }
        }
        self.dataArray = di
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.dataArray.keys.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let mean = Array(self.dataArray.keys)[section]
        return (self.dataArray[mean]?.count)!
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            Common.share.headerFontColor(label: headerView.textLabel!)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Array(self.dataArray.keys)[section]
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DefinitionTableCell", for: indexPath)

        let mean = Array(self.dataArray.keys)[indexPath.section]
        let text = self.dataArray[mean]![indexPath.row]
        
        cell.textLabel?.text = text.M
        cell.textLabel?.numberOfLines = 0
        
        cell.indentationLevel = 1

        Common.share.headerFontColor(label: cell.textLabel!, cell: true)
        cell.textLabel?.font = UIFont.systemFont(ofSize: 18)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Common.share.cellBackColor(index: indexPath.row)
    }

}
