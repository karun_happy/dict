//
//  SearchVC.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 30/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit
import Kingfisher

enum Views: String {
    case Main = "Main"
    case Dict = "Dictionary"
}

class SearchVC: UIViewController {

    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var tableViewSearch: UITableView!
    
    var arrSearch = NSArray()
    var fromView  = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tableViewSearch.isHidden = true
        self.tableViewSearch.tableFooterView = UIView()
        self.tableViewSearch.tableHeaderView = UIView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.txtSearch.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewBackground(image: UIImage) {
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    
    func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Add Search Word to History Table in Databse
    func addHistory(data: NSDictionary) {
        let d = (DictionaryModel(o: data).data)
        if d.count <= 0 { return }
        
        let words = Data(o: d)
        
        self.alreadyThere(id: words.WordId) { (count) in
            let historyInfo = SearchHistory()
            historyInfo.word     = words.Word
            historyInfo.word_id  = words.WordId
            historyInfo.lem_word = words.Word
            historyInfo.count    = count
            historyInfo.searched_at = (NSDate().timeIntervalSince1970 * 1000).description
            var isInserted = Bool()
            if count > 1 {
                isInserted = ModelManager.getInstance().updateHistory(history: historyInfo)
            } else {
                isInserted = ModelManager.getInstance().addHistory(history: historyInfo)
            }
            if isInserted {
                print("History Added")
            } else {
                print("History not Added")
            }
        }
    }
    
    func alreadyThere(id: Int, completion: (Int) -> ()) {
        let isAlreadyThere = ModelManager.getInstance().getHistory(arg: true, id: id)
        var count = 1
        if isAlreadyThere.count > 0 {
            count = (isAlreadyThere[0] as! SearchHistory).count
            count = count + 1
        }
        completion(count)
    }
    
    func searchWords(word: String) {
        self.txtSearch.text = ""
        
        API.getApi(controller: self, method: API.searchWord + word.trimmingCharacters(in: .whitespacesAndNewlines), param: [:]) { (result) in
            guard result != nil else { return }
            if (result?["success"] as! String) == "1" {
                self.addHistory(data: result!)
                self.dismissView()
                
                let res: [Any] = [result!, word.capitalized]
                var notificatonName = ""
                if self.fromView == Views.Main.rawValue {
                    notificatonName = "PostView"
                } else if self.fromView == Views.Dict.rawValue {
                    notificatonName = "UpdateView"
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificatonName), object: res)
                NotificationCenter.default.removeObserver(NSNotification.Name(rawValue: notificatonName))
            }
        }
    }
}

extension SearchVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchSuggestionCell", for: indexPath) as UITableViewCell
        
        let element = self.arrSearch[indexPath.row] as! SearchSuggestion
        cell.textLabel?.text = element.word
        
        cell.textLabel?.font = UIFont.systemFont(ofSize: 17)
        cell.textLabel?.textColor = UIColor.colorFromCode(0x737373)
        
        return cell
    }
}
extension SearchVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let element = self.arrSearch[indexPath.row] as! SearchSuggestion
        self.searchWords(word: element.word)
        //        self.displayTable(visible: true)
    }
}

//MARK: Textfield Delegate Methods
extension SearchVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.searchWords(word: textField.text!)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(self.txtSearch) {
            print(textField.text! + string)
            self.arrSearch = ModelManager.getInstance().filterSearch(character: textField.text! + string)
            print(arrSearch)
            if self.arrSearch.count > 0 {
                self.tableViewSearch.isHidden = false
                self.tableViewSearch.reloadData()
            } else {
                self.tableViewSearch.isHidden = true
            }
        }
        return true
    }
}
