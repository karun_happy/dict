//
//  ViewController.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 10/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var viewTextButton: UIView!
    @IBOutlet var titleKitkat: UILabel!
    @IBOutlet var collectionViewHistory: UICollectionView!
    var layout = FlowLayout()
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var tableViewSuggestions: UITableView!
    @IBOutlet var viewSuggestions: UIView!
    @IBOutlet var txtWordSuggestion: UITextField!
    
    var arrSeacrchedHistory = NSArray()
    var arrSearch = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Common.share.screenSize.width)
        if Common.share.screenSize.width <= 320 {
            self.titleKitkat.font = UIFont.boldSystemFont(ofSize: 18)
        }
        self.setup()
        NotificationCenter.default.addObserver(self, selector: #selector(PostView(object:)), name: NSNotification.Name(rawValue: "PostView"), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.arrSeacrchedHistory = ModelManager.getInstance().getHistory()
        self.collectionViewHistory.reloadData()
    }
    
    @objc func PostView(object: NSNotification) {
        let obj = object.object as! NSArray
        
        let vc = SegueController.DictionaryVC()
        vc.data = obj[0] as! NSDictionary
        vc.searchedWord = obj[1] as! String
        vc.titleFont = self.titleKitkat.font.pointSize
        self.navigationController?.show(vc, sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        self.viewTextButton.layer.borderWidth = 1
        self.viewTextButton.layer.borderColor = UIColor.colorFromCode(0xf2f2f2).cgColor
//        self.txtSearch.layer.borderColor = UIColor(red: 187/255, green: 186/255, blue: 193/255, alpha: 1).cgColor
//        self.txtSearch.layer.borderWidth = 1
        let spacerView = UIView(frame:CGRect(x:0, y:0, width:10, height:40))
        self.txtSearch.leftViewMode = .always
        self.txtSearch.leftView = spacerView
        
        self.layout.alignment = .center
        self.layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10)
        self.layout.headerReferenceSize = CGSize(width: 300, height: 50)
        self.layout.footerReferenceSize = CGSize(width: 300, height: 40)
        self.collectionViewHistory.collectionViewLayout = self.layout
        
        self.viewSuggestions.isHidden = true
    }
    
    func searchView(alpha: CGFloat) {
        
        let search = SegueController.SearchVC()
        search.fromView = Views.Main.rawValue
        search.viewBackground(image: Common.share.snapshot(view: self.view))
        search.modalPresentationStyle = .overFullScreen
        search.modalTransitionStyle = .crossDissolve
        self.present(search, animated: true, completion: nil)
//        if alpha == 0 {
//            self.txtSearch.resignFirstResponder()
//            self.txtWordSuggestion.resignFirstResponder()
//        } else {
//
//            let fromView = self.txtSearch.superview?.bounds
//            let toView = self.viewSuggestions.bounds
//
//            self.viewSuggestions.frame = fromView!
//            UIView.animate(withDuration: 0.23, delay: 0.0, options: .curveEaseInOut, animations: {
//                self.viewSuggestions.frame = toView
//                self.txtWordSuggestion.becomeFirstResponder()
//            }, completion: nil)
//        }
        self.viewSuggestions.alpha = 0 //CGFloat(alpha)
//        self.displayTable(visible: alpha == 0 ? false : true)
    }
    
/// TableView visibilty
    func displayTable(visible: Bool) {
        self.tableViewSuggestions.isHidden = visible
        if visible == false {
            self.tableViewSuggestions.reloadData()
        }
    }
    
    @IBAction func btnBackViewAction(_ sender: UIButton) {
        self.searchView(alpha: 0)
    }
    
    @IBAction func btnShareAction(_ sender: UIBarButtonItem) {
        let text = "Hello,\nCheck this out. I have been using KitKatWords Dictionary for long and it's nice.\nPlease visit https://goo/gl/rFA4WW and install it."
        let share = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        share.popoverPresentationController?.sourceView = self.view
        self.present(share, animated: true, completion: nil)
    }

/// Search Button Action
    @IBAction func btnSearchAction(_ sender: UIButton) {
        self.searchView(alpha: 1)
    }
    
    @IBAction func btnVoiceAction(_ sender: UIButton) {
    }

/// API to get data for Searched Word
    func searchWords(word: String) {
        self.txtSearch.text = ""
        self.txtWordSuggestion.text = ""
//        self.searchView(alpha: 0)
        
        API.getApi(controller: self, method: API.searchWord + word, param: [:]) { (result) in
            guard result != nil else { return }
            if (result?["success"] as! String) == "1" {
                self.addHistory(data: result!)
                let vc = SegueController.DictionaryVC()
                vc.data = result!
                vc.searchedWord = word.capitalized
                self.navigationController?.show(vc, sender: nil)
            }
        }
    }

/// Add Search Word to History Table in Databse
    func addHistory(data: NSDictionary) {
        let d = (DictionaryModel(o: data).data)
        if d.count <= 0 { return }

        let words = Data(o: d)

        self.alreadyThere(id: words.WordId) { (count) in
            let historyInfo = SearchHistory()
            historyInfo.word     = words.Word
            historyInfo.word_id  = words.WordId
            historyInfo.lem_word = words.Word
            historyInfo.count    = count
            historyInfo.searched_at = (NSDate().timeIntervalSince1970 * 1000).description
            var isInserted = Bool()
            if count > 1 {
                isInserted = ModelManager.getInstance().updateHistory(history: historyInfo)
            } else {
                isInserted = ModelManager.getInstance().addHistory(history: historyInfo)
            }
            if isInserted {
                print("History Added")
            } else {
                print("History not Added")
            }
        }
    }
    
    func alreadyThere(id: Int, completion: (Int) -> ()) {
        let isAlreadyThere = ModelManager.getInstance().getHistory(arg: true, id: id)
        var count = 1
        if isAlreadyThere.count > 0 {
            count = (isAlreadyThere[0] as! SearchHistory).count
            count = count + 1
        }
        completion(count)
    }

}

//MARK: Textfield Delegate Methods
extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        self.searchWords(word: textField.text!)
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.isEqual(self.txtSearch) {
            textField.resignFirstResponder()
            self.searchView(alpha: 1)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(self.txtWordSuggestion) {
            print(textField.text! + string)
            self.arrSearch = ModelManager.getInstance().filterSearch(character: textField.text! + string)
            print(arrSearch)
            self.displayTable(visible: false)
        }
        return true
    }
}

//MARK: - History CollectionView
extension ViewController: UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrSeacrchedHistory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HistoryCollectionCell", for: indexPath) as! HistoryCollectionCell
        cell.labelHistoryWord.text = (self.arrSeacrchedHistory[indexPath.row] as! SearchHistory).word.capitalized
        cell.labelHistoryWord.textAlignment = .center
        
        let color = Common.share.appColor.withAlphaComponent(0.5)
        cell.labelHistoryWord.textColor = color
        cell.layer.borderColor = UIColor.colorFromCode(0xf2f2f2).cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 10 //cell.bounds.height / 2
        cell.clipsToBounds = true
        
        return cell
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = Common.share.evaluateStringWidth((self.arrSeacrchedHistory[indexPath.row] as! SearchHistory).word, fontSize: 15.0)
        return CGSize(width: size + 30, height: 35)
    }
}
extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let word = (self.arrSeacrchedHistory[indexPath.row] as! SearchHistory).word
        self.searchWords(word: word)
    }
}
//MARK: - Search Suggestion Table
extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchSuggestionCell", for: indexPath) as UITableViewCell

        let element = self.arrSearch[indexPath.row] as! SearchSuggestion
        cell.textLabel?.text  = element.word
//        cell.imageView?.image = UIImage(named: "search_grey")
        return cell
    }
}
extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let element = self.arrSearch[indexPath.row] as! SearchSuggestion
//        self.searchWords(word: element.word)
//        self.displayTable(visible: true)
    }
}
