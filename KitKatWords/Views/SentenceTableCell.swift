//
//  SentenceTableCell.swift
//  KitKatWords
//
//  Created by Karun Aggarwal on 15/09/17.
//  Copyright © 2017 Squareloops. All rights reserved.
//

import UIKit

class SentenceTableCell: UITableViewCell {

    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelExplanation: UILabel!
    @IBOutlet var labelSentence: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelTitle.textColor = Common.share.appColor
        let fontDescriptor: UIFontDescriptor = labelSentence.font.fontDescriptor.withSymbolicTraits([.traitBold, .traitItalic])!
        labelSentence.font = UIFont(descriptor: fontDescriptor, size: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateCell(dict: NSDictionary) {
        let sentence = Sentence(o: dict)
        self.labelTitle.text = sentence.name
        self.labelExplanation.text = sentence.desc
        self.labelSentence.text = sentence.sentence
    }
}

struct Sentence {
    var dict = NSDictionary()
    init(o: NSDictionary) {
        dict = o
    }
    init() {}
    
    var name: String {
        return GetSet.string(val: dict["HM"])
    }
    
    var desc: String {
        return GetSet.string(val: dict["EMD"])
    }
    
    var sentence: String {
        return GetSet.string(val: dict["S"])
    }
}
